<?php 
class Ecom_options_model extends MY_Model
{
    public $rules;
    public $foreign_key;
    public function __construct()
    {
        parent::__construct();
        
        $this->table="ecom_options";
        $this->primary_key="id";
       // $this->foreign_key = 'cat_id';
        $this->before_create[] = '_add_created_by';
        $this->before_update[] = '_add_updated_by';
        $this->config();
        $this->forms();
        $this->relations();
    }
    protected function _add_created_by($data)
    {
        $data['created_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    }
    protected function _add_updated_by($data)
    {
        $data['updated_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    }
    
    public function config(){
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
        
    }
    public function relations() {
       
       
    }
    public function forms(){
        $this->rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required'
               
            ),
            array(
                'field' => 'desc',
                'lable' => 'Description',
                'rules' => 'trim|max_length[200]'
               
            )
        );
    }
}
?>