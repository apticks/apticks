<?php

class Ecom_category_model extends MY_Model
{
    public $rules;
    public $foreign_key;
    public function __construct()
    {
        parent::__construct();
       
            $this->table="ecom_categories";
            $this->primary_key="id";
            $this->foreign_key = 'cat_id';
            $this->before_create[] = '_add_created_by';
            $this->before_update[] = '_add_updated_by';
            $this->config();
            $this->forms();
            $this->relations();
    }
    protected function _add_created_by($data)
    {
        $data['created_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    }
    protected function _add_updated_by($data)
    {
        $data['updated_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    } 
    
    public function config(){
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
        
    }
    public function relations() {
        $this->has_many_pivot['brands'] = array(
            'foreign_model' => 'Ecom_brand_model',
            'pivot_table' => 'ecom_subcategories_brands',
            'local_key' => 'id',
            'pivot_local_key' => 'cat_id',
            'pivot_foreign_key' => 'brand_id',
            'foreign_key' => 'id',
            'get_relate' => FALSE
        );
        $this->has_many['ecom_sub_categories'] = array(
            'foreign_model' => 'Ecom_sub_category_model',
            'foreign_table' => 'ecom_sub_categories',
            'local_key' => 'id',
            'foreign_key' => 'cat_id',
            'get_relate' => FALSE
        );
      
       
    }
    public function forms(){
        $this->rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required|min_length[3]',
                'errors' =>  array(
                    'required' => 'You must provide a %s.',
                    'min_length' => 'you need to give minimum 3 characters'
                )
            ),
            array(
                'field' => 'desc',
                'lable' => 'Description',
                'rules' => 'trim|required|max_length[200]',
                'errors' => array(
                    'required' => 'You must provide a %s.'
                )
            )
        );
    }
}

