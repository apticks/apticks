<?php

class Travel_veh_acc_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
       
            $this->table="travel_veh_acc";
            $this->primary_key="id";
            $this->before_create[] = '_add_created_by';
            $this->before_update[] = '_add_updated_by';
            $this->config();
            $this->forms();
            $this->relations();
    }
    protected function _add_created_by($data)
    {
        $data['created_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    }
    protected function _add_updated_by($data)
    {
        $data['updated_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    } 
    public function config(){
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
        
    }
    public function relations() {
       
    }
    public function forms(){
        
    }
}

