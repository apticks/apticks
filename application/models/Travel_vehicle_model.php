<?php

class Travel_vehicle_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
       
            $this->table="travel_vehicles";
            $this->primary_key="id";
            $this->before_create[] = '_add_created_by';
            $this->before_update[] = '_add_updated_by';
            $this->config();
            $this->forms();
            $this->relations();
    }
    protected function _add_created_by($data)
    {
        $data['created_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    }
    protected function _add_updated_by($data)
    {
        $data['updated_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    } 
    public function config(){
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
        
    }
    public function relations() {
       $this->has_one['brands'] = array('Travel_brand_model', 'id', 'brand_id');
       /*$this->has_one['accessories'] = ['Travel_accessorie_model', 'id', 'accessorie_id'];*/
       $this->has_many_pivot['accessories'] = array(
            'foreign_model' => 'Travel_accessorie_model',
            'pivot_table' => 'travel_veh_acc',
            'local_key' => 'id',
            'pivot_local_key' => 'vehicle_id',
            'pivot_foreign_key' => 'accessorie_id',
            'foreign_key' => 'id',
            'get_relate' => FALSE
        );
    }
    public function forms(){
        $this->rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.'
                )
            ),
            array(
                'field' => 'brand_id',
                'label' => 'Brand',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.'
                )
            ),array(
                'field' => 'model_year',
                'label' => 'Model Year',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.'
                )
            ),
            array(
                'field'=>'desc',
                'label'=>'Vehicle Details',
                'rules'=>'trim|required',
                'erors'=>array(
                   'required' => 'You must provide a %s.'
                )
            ),
            array(
                'field' => 'no_of_vehicles',
                'label' => 'No.Of Vehicles',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.'
                )
            ),
            array(
                'field' => 'price_per_day',
                'label' => 'Price Per Day (in RS)',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.'
                )
            ),
            array(
                'field' => 'price_per_km',
                'label' => 'Price Per KM (in RS)',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.'
                )
            ),
            array(
                'field' => 'seating',
                'label' => 'Seating Capacity',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.'
                )
            ),array(
                'field' => 'fuel',
                'label' => 'Fuel Type',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.'
                )
            )

        );
    }
}

