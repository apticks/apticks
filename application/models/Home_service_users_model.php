<?php

class Home_service_users_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
       
            $this->table="home_service_users";
            $this->primary_key="id";
            $this->before_create[] = '_add_created_by';
            $this->before_update[] = '_add_updated_by';
            $this->config();
            $this->forms();
            $this->relations();
    }
    protected function _add_created_by($data)
    {
        $data['created_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    }
    protected function _add_updated_by($data)
    {
        $data['updated_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    } 
    public function config(){
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
        
    }
    public function relations() {
       $this->has_one['service_type'] = array('Home_service_type_model', 'id', 'type_id');
    }
    public function forms(){
        $this->rules = array(
            array(
                'field' => 'type_id',
                'label' => 'Service Type',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.'
                )
            ),
            array(
                'field' => 'mobile',
                'label' => 'Mobile Number',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.'
                )
            ),array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required',
                'errors' =>  array(
                    'required' => 'You must provide a %s.'
                )
            ),
            array(
                'field'=>'status',
                'label'=>'Status',
                'rules'=>'trim|required',
                'errors' =>  array(
                    'required' => 'You must select %s.'
                )
            )

        );
    }
}

