<?php

class Ecom_product_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
       
            $this->table="ecom_products";
            $this->primary_key="id";
            $this->before_create[] = '_add_created_by';
            $this->before_update[] = '_add_updated_by';
            $this->config();
            $this->forms();
            $this->relations();
    }
    protected function _add_created_by($data)
    {
        $data['created_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    }
    protected function _add_updated_by($data)
    {
        $data['updated_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    } 
    public function config(){
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
        
    }
    public function relations() {
       $this->has_one['vendor'] = ['User_model', 'id', 'vendor_id'];
       $this->has_one['category'] = ['Ecom_category_model', 'id', 'cat_id'];
       $this->has_one['sub_category'] = ['Ecom_sub_category_model', 'id', 'sub_cat_id'];
       $this->has_one['sub_sub_category'] = ['Ecom_sub_sub_category_model', 'id', 'sub_sub_cat_id'];
       $this->has_one['brand'] = ['Ecom_brand_model', 'id', 'brand_id'];
    }
    public function forms(){
        $this->rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required|min_length[5]',
                'errors' =>  array(
                    'required' => 'You must provide a %s.',
                    'min_length' => 'you need to give minimum 5 characters'
                )
            ),
           
        );
    }
    
    public function all($limit = NULL, $offset = NULL, $sub_cat_id = NULL, $search = NULL )
    {
        $cache_name = $sub_cat_id . $search . $limit . $offset;
        $this->set_cache($cache_name); //just to set cache_name using MY_model
        $result     = $this->_get_from_cache(); //MY_model
        
        if ( ! (isset($result) && $result !== FALSE))
        {
            $this->_query_all($sub_cat_id, $search);
            $this->db->order_by('name', 'ASC');
            if ( $limit != NULL && $offset != NULL)
            {
                $this->db->limit($limit, $offset);
            }
            $rs     = $this->db->get($this->table);
            $result = $rs->custom_result_object('Products_list_row');
            $this->_write_to_cache($result); //MY_model
        }
        //print_array($this->db->last_query());
        
        $this->db->reset_query();
        
        $this->_query_all($sub_cat_id, $search);
        $count = $this->db->count_all_results($this->table);
        
        return  array(
            'result' => $result,
            'count'  => $count
        );
    }
    
    private function _query_all($sub_cat_id = NULL, $search = NULL)
    {
        
        $this->load->model(array('ecom_sub_category_model'));
        
        $ecom_sub_category_table       = '`' . $this->ecom_sub_category_model->table . '`';
        $ecom_sub_category_primary_key = '`' . $this->ecom_sub_category_model->primary_key . '`';
        $ecom_sub_category_foreign_key = '`' . $this->ecom_sub_category_model->foreign_key . '`';
        
        $primary_key = '`' . $this->primary_key . '`';
        $table       = '`' . $this->table . '`';
        
        $str_select_product = '';
        foreach (array( 'id', 'name', 'mrp', 'offer_price') as $v)
        {
            $str_select_product .= "$table.`$v`,";
        }
        
        $this->db->select($str_select_product);
        $this->db->join($ecom_sub_category_table, "$ecom_sub_category_table.$primary_key=$table.$ecom_sub_category_foreign_key");
        
        if ($sub_cat_id)
        {
            $this->db->where("$ecom_sub_category_table.$ecom_sub_category_primary_key=", $sub_cat_id);
        }
        
        if ( ! is_null($search))
        {
            
            $this->db->or_like($table . '.`sounds_like`', metaphone($search));
        }
        $this->db->where("$table.`status`=", '1');
        return $this;
    }
    
}

class Products_list_row
{
    public $id;
    public $name;
    public $mrp;
    public $offer_price;
}