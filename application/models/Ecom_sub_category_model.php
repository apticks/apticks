 <?php

class Ecom_sub_category_model extends MY_Model
{
    public $rules;
    public $foreign_key;
    public function __construct()
    {
        parent::__construct();
        $this->table="ecom_sub_categories";
        $this->primary_key = "id";
        $this->foreign_key = 'sub_cat_id';
        $this->before_create[] = '_add_created_by';
        $this->before_update[] = '_add_updated_by';
        
        
        $this->_config();
        $this->_form();
        $this->_relations();
        
    }
    protected function _add_created_by($data)
    {
        $data['created_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    }
    protected function _add_updated_by($data)
    {
        $data['updated_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    }
    public function _config(){
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
        
    }
    public function _relations()
    {  
        $this->has_many_pivot['brands'] = array(
        'foreign_model' => 'Ecom_brand_model',
        'pivot_table' => 'ecom_subcategories_brands',
        'local_key' => 'id',
        'pivot_local_key' => 'sub_cat_id',
        'pivot_foreign_key' => 'brand_id',
        'foreign_key' => 'id',
        'get_relate' => FALSE
       );
        
        $this->has_many['ecom_sub_sub_categories'] = array(
            'foreign_model' => 'Ecom_sub_sub_category_model',
            'foreign table' => 'ecom_sub_sub_categories',
            'foreign_key' =>'sub_cat_id',
            'local_key' =>'id',
            'get_relate' => FALSE
        );
       
    }
    public function _form(){
        $this->rules = array(
            array(
                'field'=>'cat_id',
                'label'=>'Category Id',
                'rules'=>'trim|required',
                'errors'=>array(
                    'required'=>'Please select category'
                )
            ),
            array(
                'field'=>'name',
                'label'=>'Name',
                'rules'=>'trim|required|min_length[3]',
                'errors'=>array(
                    'min_length'=>'Please give minimum 3 characters'
                )
            ),
            array(
                'field'=>'desc',
                'label'=>'Description',
                'rules'=>'trim|required|max_length[200]',
                'erors'=>array(
                    'max_length'=>'You can give maximum 200 characters'
                )
                
            )
        );
    }
}
?>