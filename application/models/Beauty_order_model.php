<?php

class Beauty_order_model extends MY_Model
{
    public $rules;
    public $foreign_key;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'beauty_orders';
        $this->primary_key = 'id';
        $this->foreign_key = 'package_id';
       $this->_config();
       $this->_form();
       $this->_relations();
    }
   
    public function _config() {
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
        
    }
    
    public function _relations(){
        $this->has_many['packages'] = array(
            'foreign_model' => 'beauty_package_model',
            'foreign_table' => 'beauty_packages',
            'local_key' => 'package_id',
            'foreign_key' => 'id',
            'get_relate' => FALSE
        );
       
    }
    
    public function _form(){
        $this->rules = array(
            array(
                'field' => 'package_id',
                'lable' => 'package_id',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'order_id',
                'lable' => 'order_id',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'qty',
                'lable' => 'Quantity',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'price',
                'lable' => 'Price',
                'rules' => 'trim|required',
            ),
            );
    }
    
    
   
    
}

