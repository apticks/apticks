<?php

class Home_service_order_model extends MY_Model
{
    public $rules;
    public $foreign_key;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'home_service_orders';
        $this->primary_key = 'id';
        $this->foreign_key = 'order_id';
        
       $this->_config();
       $this->_form();
       $this->_relations();
    }
    public function _config() {
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
    }
    
    public function _relations(){
        $this->has_one['address'] = array('Users_address_model', 'address_id', 'id');
        $this->has_one['user'] = array('User_model', 'id', 'user_id');
        $this->has_one['vendor'] = array('Vendor_list_model', 'id', 'vendor_id');

        $this->has_many['order_service'] = array(
            'foreign_model' => 'Home_service_ord_ser_model',
            'foreign_table' => 'home_service_ord_ser',
            'local_key' => 'id',
            'foreign_key' => $this->foreign_key,
            'get_relate' => FALSE
        );
       /* $this->has_many['sub_order_items'] = array(
            'foreign_model' => 'Food_sub_order_items_model',
            'foreign_table' => 'food_sub_order_items',
            'local_key' => 'id',
            'foreign_key' => $this->foreign_key,
            'get_relate' => FALSE
        );*/
    }
    
   
    
    public function _form(){
        $this->rules = array(
            /*array(
                'field' => 'discount',
                'lable' => 'discount',
                'rules' => 'required',
            ),
            array(
                'field' => 'tax',
                'lable' => 'tax',
                'rules' => 'required',
            ),
            array(
                'field' => 'total',
                'lable' => 'total',
                'rules' => 'required',
            ),
            array(
                'field' => 'coupon_id',
                'lable' => 'coupon id',
                'rules' => 'required',
            ),
            array(
                'field' => 'payment_method_id',
                'lable' => 'payment method id',
                'rules' => 'required',
            ),*/
            array(
                'field' => 'date',
                'lable' => 'Date',
                'rules' => 'required',
            ),
            array(
                'field' => 'time',
                'lable' => 'Time',
                'rules' => 'required',
            ),
            array(
                'field' => 'address_id',
                'lable' => 'Address',
                'rules' => 'required',
            ),
        );
    }
}

