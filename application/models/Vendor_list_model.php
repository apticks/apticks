<?php

class Vendor_list_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'vendors_list';
        $this->primary_key = 'id';
        $this->before_create[] = '_add_created_by';
        $this->before_update[] = '_add_updated_by';
        
       $this->_config();
       $this->_form();
       $this->_relations();
    }
    protected function _add_created_by($data)
    {
        $data['created_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    }
    protected function _add_updated_by($data)
    {
        $data['updated_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    }
    public function _config() {
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
    }
    
    public function _relations(){
        $this->has_one['location'] = array('Location_model', 'id', 'location_id');
        $this->has_one['executive'] = array('User_model', 'id', 'executive_id');
        $this->has_one['category'] = array('Category_model', 'id', 'category_id');
        $this->has_one['constituency'] = array('Constituency_model', 'id', 'constituency_id');
        $this->has_many['contacts'] = array(
            'foreign_model' => 'Contact_model',
            'foreign_table' => 'contacts',
            'local_key' => 'id',
            'foreign_key' => 'list_id'
        );
        $this->has_many['links'] = array(
            'foreign_model' => 'Social_model',
            'foreign_table' => 'social',
            'local_key' => 'id',
            'foreign_key' => 'list_id'
        );
        $this->has_many_pivot['amenities'] = array(
            'foreign_model' => 'Amenity_model',
            'pivot_table' => 'vendor_amenities',
            'local_key' => 'id',
            'pivot_local_key' => 'list_id',
            'pivot_foreign_key' => 'amenity_id',
            'foreign_key' => 'id'
        );
        $this->has_many_pivot['services'] = array(
            'foreign_model' => 'Service_model',
            'pivot_table' => 'vendor_services',
            'local_key' => 'id',
            'pivot_local_key' => 'list_id',
            'pivot_foreign_key' => 'service_id',
            'foreign_key' => 'id'
        );
        $this->has_many_pivot['holidays'] = array(
            'foreign_model' => 'Day_model',
            'foreign_table' => 'days',
            'pivot_table' => 'vendors_holidays',
            'local_key' => 'id',
            'pivot_local_key' => 'list_id',
            'foreign_key' => 'id',
            'pivot_foreign_key' => 'day_id',
        );
        $this->has_many['timings'] = array(
          'foreign_model' => 'vendor_timings_model',
          'foreign_table' => 'vendor_timings',
          'local_key' => 'id',
          'foreign_key' => 'list_id'
        );
        
    }
    
    public function _form(){
        $this->rules = array(
            array(
                'field' => 'name',
                'lable' => 'Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'email',
                'lable' => 'Email',
                'rules' => 'trim|required|valid_email|callback_check_email',
                'errors' => array(
                    'callback_check_email' =>'email already exists'
                )
            ),
            array(
                'field' => 'constituency_id',
                'lable' => 'Constituency Id',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'category_id',
                'lable' => 'Category Id',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'address',
                'lable' => 'Address',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'landmark',
                'lable' => 'Landmark',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'pincode',
                'lable' => 'Pincode',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'cover',
                'lable' => 'Cover Image',
                'rules' => 'trim|required'
            ),
        );
    }
    
    public function all($limit = NULL, $offset = NULL, $cat_id = NULL, $sub_cat_id = NULL, $search = NULL, $lat = FALSE, $long = NULL)
    {
        $cache_name = $cat_id. $sub_cat_id . $search . $lat . $long . $limit . $offset;
        $this->set_cache($cache_name); //just to set cache_name using MY_model
        $result     = $this->_get_from_cache(); //MY_model
        
        if ( ! (isset($result) && $result !== FALSE))
        {
            $this->_query_all($cat_id, $sub_cat_id);
            $this->db->order_by('name', 'ASC');
            $this->db->order_by('created_at', 'DESC');
            $this->db->order_by('updated_at', 'DESC');
            if ( $limit != NULL && $offset != NULL)
            {
                $this->db->limit($limit, $offset);
            }
            $rs     = $this->db->get($this->table);
            $result = $rs->custom_result_object('Vendor_list_row');
            $this->_write_to_cache($result); //MY_model
        }
        
        
        $this->db->reset_query();
        
        $this->_query_all($cat_id, $sub_cat_id);
        //print_array($this->db->last_query());
        $count = $this->db->count_all_results($this->table);
        return  array(
            'result' => $result,
            'count'  => $count
        );
    }
    
    private function _query_all($cat_id = NULL,$sub_cat_id = NULL, $search = NULL, $lat = NULL, $long = NULL)
    {
        
        $this->load->model(array('location_model', 'category_model', 'sub_category_model'));
        
        $location_table       = '`' . $this->location_model->table . '`';
        $location_primary_key = '`' . $this->location_model->primary_key . '`';
        $location_foreign_key = '`' . 'location_id' . '`';
        
        $category_table       = '`' . $this->category_model->table . '`';
        $category_primary_key = '`' . $this->category_model->primary_key . '`';
        $category_foreign_key = '`' . 'category_id' . '`';
        
        $primary_key = '`' . $this->primary_key . '`';
        $table       = '`' . $this->table . '`';
        
        $str_select_vendor = '';
        foreach (array('created_at', 'updated_at', 'created_user_id', 'updated_user_id', 'id', 'name', 'email', 'unique_id', 'address', 'landmark',) as $v)
        {
            $str_select_vendor .= "$table.`$v`,";
        }
        
        $this->db->select($str_select_vendor."$location_table.`latitude`, $location_table.`longitude`, $location_table.`address` as location_address");
        $this->db->join($category_table, "$category_table.$primary_key=$table.$category_foreign_key");
        $this->db->join($location_table,"$location_table.$primary_key=$table.$location_foreign_key");
        $this->db->join('`vendors_sub_categories`', "$table.$primary_key=vendors_sub_categories.list_id");
        if ($cat_id)
        {
            $this->db->where("$category_table.$category_primary_key=", $cat_id);
        }
        if ($sub_cat_id)
        {
            $this->db->where("`vendors_sub_categories`.`sub_category_id`=", $sub_cat_id);
        }
        
        if ( ! is_null($search))
        {
            
            $this->db->or_like($table . '.`student_school_id`', $search);
            $this->db->or_like($table . '.`student_lastname`', $search);
            $this->db->or_like($table . '.`student_firstname`', $search);
            $this->db->or_like($table . '.`student_middlename`', $search);
        }
        $this->db->where("$table.`status`=", '1');
        return $this;
    }
    
}
class Vendor_list_row
{
    public $id;
    public $name;
    public $email;
    public $unique_id;
    public $address;
    public $landmark;
    public $created_at;
    public $updated_at;
    public $created_user_id;
    public $updated_user_id;
    
}


