<?php

class Ecom_cart_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
       
            $this->table="ecom_cart";
            $this->primary_key="id";
            $this->config();
            $this->forms();
            $this->relations();
    }
   
    public function config(){
        $this->timestamps = TRUE;
        $this->soft_deletes = FALSE;
        $this->delete_cache_on_save = TRUE;
        
    }
    public function relations() {
       $this->has_one['product'] = array('ecom_product_model', 'id', 'product_id');
    }
    public function forms(){
        $this->rules = array(
            array(
                'field' => 'product_id',
                'label' => 'Product Id',
                'rules' => 'required',
            ),
            array(
                'field' => 'qty',
                'label' => 'Quantity',
                'rules' => 'required',
            ),
            
        );
    }
}

