<?php

class Wallet_transaction_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'wallet_transactions';
        $this->primary_key = 'id';
        
        $this->_config();
        $this->_form();
        $this->_relations();
    }
    
    public function _config(){
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
    }
    
    
    public function _relations() {
        $this->has_one['bank'] = array('Bank_details_model', 'id', 'bank_id');
    }
    
    public function _form(){
        
    }
}

