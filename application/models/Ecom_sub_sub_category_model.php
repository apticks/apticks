 <?php

class Ecom_sub_sub_category_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
        $this->table="ecom_sub_sub_categories";
        $this->primary_key = "id";
        $this->before_create[] = '_add_created_by';
        $this->before_update[] = '_add_updated_by';
        
        
        $this->_config();
        $this->_form();
        $this->_relations();
        
    }
    protected function _add_created_by($data)
    {
        $data['created_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    }
    protected function _add_updated_by($data)
    {
        $data['updated_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    }
    public function _config(){
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
        
    }
    public function _relations()
    {  
        $this->has_one['category'] = ['Ecom_category_model', 'id', 'cat_id'];
        $this->has_one['sub_category'] = ['Ecom_sub_category_model', 'id', 'sub_cat_id'];
        
        $this->has_many_pivot['options'] = array(
            'foreign_model' => 'ecom_options_model',
            'pivot_table' => 'ecom_sub_sub_categories_options',
            'local_key' => 'id',
            'pivot_local_key' => 'sub_sub_cat_id',
            'pivot_foreign_key' => 'option_id',
            'foreign_key' => 'id',
            'get_relate' => FALSE
        );
    }
    public function _form(){
        $this->rules = array(
            array(
                'field'=>'cat_id',
                'label'=>'Category Id',
                'rules'=>'trim|required',
                'errors'=>array(
                    'required'=>'Please select category'
                )
            ),
            array(
                'field'=>'name',
                'label'=>'Name',
                'rules'=>'trim|required|min_length[5]',
                'errors'=>array(
                    'min_length'=>'Please give minimum 5 characters'
                )
            ),
            array(
                'field'=>'desc',
                'label'=>'Description',
                'rules'=>'trim|required|max_length[200]',
                'erors'=>array(
                    'max_length'=>'You can give maximum 200 characters'
                )
                
            )
        );
    }
}
?>