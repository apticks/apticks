<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Apticks</title>
	<?php $this->load->view('template/master/topcss');?>
</head>
<body>
<!-- navbar area start -->
	<?php $this->load->view('template/master/header');?>
<!-- navbar area end -->

<!-- Content area start -->
	<?php $this->load->view($content);?>
<!-- Content area end -->

<!-- footer area start -->
	<?php $this->load->view('template/master/footer');?>
<!-- footer area end -->

<!-- back to top area start -->
<div class="back-to-top">
    <span class="back-top"><i class="fa fa-angle-up"></i></span>
</div>
<!-- back to top area end -->

<?php $this->load->view('template/master/scripts');?>
</body>
</html>
