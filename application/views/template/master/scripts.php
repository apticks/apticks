<!-- jquery -->
<script src="<?php echo base_url();?>assets/master/js/jquery-2.2.4.min.js"></script>
<!-- popper -->
<script src="<?php echo base_url();?>assets/master/js/popper.min.js"></script>
<!-- bootstrap -->
<script src="<?php echo base_url();?>assets/master/js/bootstrap.min.js"></script>
<!-- magnific popup -->
<script src="<?php echo base_url();?>assets/master/js/jquery.magnific-popup.js"></script>
<!-- wow -->
<script src="<?php echo base_url();?>assets/master/js/wow.min.js"></script>
<!-- owl carousel -->
<script src="<?php echo base_url();?>assets/master/js/owl.carousel.min.js"></script>
<!-- slick slider -->
<script src="<?php echo base_url();?>assets/master/js/slick.js"></script>
<!-- cssslider slider -->
<script src="<?php echo base_url();?>assets/master/js/jquery.cssslider.min.js"></script>
<!-- waypoint -->
<script src="<?php echo base_url();?>assets/master/js/waypoints.min.js"></script>
<!-- counterup -->
<script src="<?php echo base_url();?>assets/master/js/jquery.counterup.min.js"></script>
<!-- imageloaded -->
<script src="<?php echo base_url();?>assets/master/js/imagesloaded.pkgd.min.js"></script>
<!-- isotope -->
<script src="<?php echo base_url();?>assets/master/js/isotope.pkgd.min.js"></script>
<!-- world map -->
<script src="<?php echo base_url();?>assets/master/js/worldmap-libs.js"></script>
<script src="<?php echo base_url();?>assets/master/js/worldmap-topojson.js"></script>
<!-- main js -->
<script src="<?php echo base_url();?>assets/master/js/main.js"></script>
<script src="<?php echo base_url();?>assets/js/site.js"></script>