

<!-- preloader area start -->
<div class="preloader" id="preloader">
    <div class="preloader-inner">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
</div>
<!-- preloader area end -->

<!-- search Popup -->
<div class="body-overlay" id="body-overlay"></div>
<div class="search-popup" id="search-popup">
    <form action="https://codingeek.net/html/riyaqas/index.html" class="search-form">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search.....">
        </div>
        <button type="submit" class="submit-btn"><i class="fa fa-search"></i></button>
    </form>
</div>
<!-- //. search Popup -->

<nav class="navbar navbar-area navbar-expand-lg nav-style-01">
    <div class="container nav-container">
        <div class="responsive-mobile-menu">
            <div class="logo-wrapper mobile-logo">
                <a href="<?php echo base_url();?>home" class="logo">
                    <img src="<?php echo base_url();?>assets/img/logoresize.png" alt="logo">
                </a>
            </div>
            <div class="nav-right-content">
                <ul>
                    
                    <li class="cart" >
                        
                        <div class="widget_shopping_cart_content">
                            <ul>
                                <li>
                                    <div class="media">
                                        <div class="media-left">
                                            <img src="<?php echo base_url();?>assets/master/img/checkout/7.png" alt="img">
                                        </div>
                                        <div class="media-body">
                                            <a class="title" href="#">Smart watch red color</a>
                                            <p>Quantity: 1</p>
                                            <span class="price">$150.00</span>
                                        </div>
                                    </div>
                                    <a class="remove-product" href="#"><span class="ti-close"></span></a>
                                </li>
                                <li>
                                    <div class="media">
                                        <div class="media-left">
                                            <img src="<?php echo base_url();?>assets/master/img/checkout/8.png" alt="img">
                                        </div>
                                        <div class="media-body">
                                            <a class="title" href="#">Smart watch red color</a>
                                            <p>Quantity: 1</p>
                                            <span class="price">$150.00</span>
                                        </div>
                                    </div>
                                    <a class="remove-product" href="#"><span class="ti-close"></span></a>
                                </li>
                            </ul>
                            <p class="total">
                                <strong>Subtotal:</strong> 
                                <span class="amount">
                                    <span class="woocommerce-Price-currencySymbol">$</span>129.00
                                </span>       
                            </p>
                            <p class="buttons">
                                <a href="checkout.html" class="button">View Card & Check out</a>
                            </p>
                        </div>
                    </li>
                   
                </ul>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#riyaqas_main_menu" 
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggle-icon">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="riyaqas_main_menu">
            <div class="logo-wrapper desktop-logo">

                <a href="<?php echo base_url();?>home" class="logo resize">
                    <img src="<?php echo base_url();?>assets/img/logonew.svg" alt="logo" style="width: 250px; height: 100px;">
                </a>

            </div>
            <ul class="navbar-nav zoom ">
                <li>
                    <a href="<?php echo base_url();?>home"><span class="about">Home</span></a>
                </li>
                <li>
                   <a href="<?php echo base_url();?>about_us"><span class="about">About Us</span></a>
                </li> 
                <li class="menu-item-has-children">
                    <a href="#"><span class="about">Services</span></a>
                    <ul class="sub-menu">
                        <li><a href="<?php echo base_url();?>software">Software Development</a></li>
                        <li><a href="<?php echo base_url();?>mobile">Mobile App Development</a></li>
                        <li><a href="<?php echo base_url();?>web">Web Development</a></li>
                        <li><a href="<?php echo base_url();?>digital">Digital Marketing</a></li>
                        <li><a href="<?php echo base_url();?>graphic">Graphics Design</a></li>
                        <li><a href="<?php echo base_url();?>testing">Software Testing</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="about">Portfolio</span></a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>contact"><span class="about">Contact</span></a>
                </li>
            </ul>
        </div>
<!--         <div class="nav-right-content">
            <ul>
                <li class="search">
                    <i class="ti-search"></i>
                </li>
                <li class="cart" >
                    <div class="cart-icon"><i class="ti-shopping-cart"></i></div>
                    <div class="widget_shopping_cart_content">
                        <ul>
                            <li>
                                <div class="media">
                                    <div class="media-left">
                                        <img src="<?php echo base_url();?>assets/master/img/checkout/7.png" alt="img">
                                    </div>
                                    <div class="media-body">
                                        <a class="title" href="#">Smart watch red color</a>
                                        <p>Quantity: 1</p>
                                        <span class="price">$150.00</span>
                                    </div>
                                </div>
                                <a class="remove-product" href="#"><span class="ti-close"></span></a>
                            </li>
                            <li>
                                <div class="media">
                                    <div class="media-left">
                                        <img src="<?php echo base_url();?>assets/master/img/checkout/8.png" alt="img">
                                    </div>
                                    <div class="media-body">
                                        <a class="title" href="#">Smart watch red color</a>
                                        <p>Quantity: 1</p>
                                        <span class="price">$150.00</span>
                                    </div>
                                </div>
                                <a class="remove-product" href="#"><span class="ti-close"></span></a>
                            </li>
                        </ul>
                        <p class="total">
                            <strong>Subtotal:</strong> 
                            <span class="amount">
                                <span class="woocommerce-Price-currencySymbol">$</span>129.00
                            </span>       
                        </p>
                        <p class="buttons">
                            <a href="checkout.html" class="button">View Card & Check out</a>
                        </p>
                    </div>
                </li>
                 <li class="notification">
                    <a href="#">
                        <i class="fa fa-heart-o"></i>
                        <span class="notification-count">0</span>
                    </a>
                </li> 
            </ul>
        </div> -->
    </div>
</nav>
<!--<style>


.zoom {
  padding: 50px;

  transition: transform .2s;
  width: 200px;
  height: 200px;
  margin: 0 auto;
}

.zoom:hover {
  -ms-transform: scale(1.5); /* IE 9 */
  -webkit-transform: scale(1.5); /* Safari 3-8 */
  transform: scale(1.5); 
}
</style>-->
<style type="text/css">
    
    .zoom{
        font-family: tinos;
         transition: transform .2s;
 
    }

    .zoom:hover {
  -ms-transform: scale(1.5); /* IE 9 */
  -webkit-transform: scale(1.5); /* Safari 3-8 */
  transform: scale(1.1,1.1); 
  margin-right: 0px;
}

</style>