<!-- favicon -->
<link rel=icon href="<?php echo base_url();?>assets/master/img/apti.png" sizes="20x20"
	type="image/png">
<!-- animate -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/animate.css">
<!-- bootstrap -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/bootstrap.min.css">
<!-- magnific popup -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/magnific-popup.css">
<!-- owl carousel -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/owl.carousel.min.css">
<!-- fontawesome -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/themify-icons.css">
<!-- flaticon -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/flaticon.css">
<!-- slick slider -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/slick.css">
<!-- animated slider -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/animated-slider.css">
<!-- Main Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/style.css">
<!-- responsive Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/responsive.css">

<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Tinos" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/master/css/progress-tracker.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/master/css/site.css">