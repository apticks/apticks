<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'vendor/autoload.php';
class Welcome extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public $template;
	function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        
        $this->load->library('table');
        $this->template = array(
            'table_open'            => '<table border="1" cellpadding="2" cellspacing="1" class="mehartable">',
            
            'thead_open'            => '<thead>',
            'thead_close'           => '</thead>',
            
            'heading_row_start'     => '<tr>',
            'heading_row_end'       => '</tr>',
            'heading_cell_start'    => '<th>',
            'heading_cell_end'      => '</th>',
            
            'tbody_open'            => '<tbody>',
            'tbody_close'           => '</tbody>',
            
            'row_start'             => '<tr>',
            'row_end'               => '</tr>',
            'cell_start'            => '<td>',
            'cell_end'              => '</td>',
            
            'row_alt_start'         => '<tr>',
            'row_alt_end'           => '</tr>',
            'cell_alt_start'        => '<td>',
            'cell_alt_end'          => '</td>',
            
            'table_close'           => '</table>'
        );
        $this->table->set_template($this->template);
    }
	public function index()
	{ 
	    $details = array(array('Name' =>'Fred', 'Color' =>'Blue', 'Size' =>'Small'), array('Name' =>'Mary', 'Color' =>'Red', 'Size' =>'Large'), array('Name' =>'John', 'Color' =>'Green', 'Size' =>'Medium'));
	    echo $this->table->generate($details);
	}
}
