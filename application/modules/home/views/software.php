<!-- breadcrumb area start -->
<div class="breadcrumb-area" style="background-image:url(assets/master/img/page-title-bg.png);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title about">Software Development</h1>
                    <ul class="page-list">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li>Software Development</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- content area Start -->

<div class="sbst-offer-area pd-top-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="img-wrap">
                        <img src="<?php echo base_url();?>assets/img/outsource.png" alt="video" style="margin-top: 72px;">
                        
                    </div>            
            </div>
            <div class="col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two">
                         <h2 class="title">Software <span>Development</span></h2>
                        <p class="about">Software developer, also referred to as a programmer , you’ll be playing a key role within the design, installation, testing and maintenance of software systems. The creative minds behind computer programs. Some develop the applications that allow people to try to to specific tasks on a computer or another device. Others develop the underlying systems that run the devices or that control networks. Software development managers lead teams of software developers working in industries starting from medical research to finance. additionally to designing software, web applications, and web services. The Importance of Software Development. once we check out any automated system the trouble of building is sort of agile and simply but behind this an enormous mind works which is developed by a software team.</p>
                </div>
                    
                </div>
            </div>
        </div>
    </div>
<br>
<br>

    <div class="container">
        <div class="row">
            <div class="col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title about">Software <span>Outsourcing</span></h2>
                        <p  class="about">Software outsourcing essentially describes a situation during which a corporation chooses to rent a third-party programmer to supply services associated with software development. By handing out essential business processes over to a specialized third-party, companies are then ready to manage even the foremost complex tasks. consistent with current statistics, a minimum of 60% of the entire outsourcing market is comprised of IT/software workers. It is sensible to rent developers for variety of reasons:




</p>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="riyaqas-check-list about">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span class="about">
It’s hard to seek out a business that doesn’t need software services</span>
                            </div>
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span class="about">Software projects are constantly increasing</span>
                            </div>
                             <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span class="about">Companies can reduce costs significantly by outsourcing software development services</span>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
            <div class="col-lg-6 wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
             
                    <div class="img-wrap">
                        <img src="<?php echo base_url();?>assets/img/softfinal.png" alt="video">
                        
                    </div>
                
            
            </div>
            
        </div>
    </div>  </div>

<!-- Content area End -->
<style type="text/css">
   .about{
    font-family: tinos;
    font-size: 20px;
   }
</style>