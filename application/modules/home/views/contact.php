<!-- breadcrumb area start -->
<div class="breadcrumb-area" style="background-image:url(assets/master/img/page-title-bg.png)";>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">Contact</h1>
                    <ul class="page-list">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li>Contact</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->
<!-- map area start -->
<div class="map-area pd-top-120">
    <div class="container">
        <div class="map-area-wrap">
            <div class="row no-gutters">
                 <div class="col-xl-7 col-lg-6">
                 <div class="img-with-video">
                    <div class="img-wrap">
                        <img src="<?php echo base_url();?>assets/master/img/contact.gif" alt="video" style="margin-top: 100px;">
                     </div>
                 </div>
            </div> 
                <div class="col-lg-4 desktop-center-item">
                    <div>
                        <div class="contact-info">
                            <h4 class="title">Contact info:</h4>
                            <p class="sub-title">We hope you get pleasure from our services the maximum amount as we tend to get pleasure from giving them to you.</p>
                            <p>Address: Plot No.86,Street Number 6,<br>Patrika Nagar,<br>HIGHTECH City,<br>Hyderabad,Telangana,<br>500081</p>
                            <p><span>Mobile:</span> +091 8522808784</p>
                            <p><span>E-mail:</span>  info@apticks.com</p>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>
<!-- map area End -->
<!-- contact form start -->
<div class="contact-form-area pd-top-112">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8">
                <div class="section-title text-center w-100">
                    <h2 class="title">Send you <span>inquiry</span></h2>
                    <p>A live person to answer you every time you call: Assured !</p>
                    <p>Professionals at Apticks ar instantly accessible by phone for support and consultation. Our help desk Service offers you unlimited, live phone access to skillful and authorized technicians instantly.

</p>
                </div>
            </div>
            <div class="col-lg-9">
               <form class="needs-validation" novalidate="" action="<?php echo base_url('contact');?>" method="post" enctype="multipart/form-data">
		
                    <div class="row custom-gutters-16">
                        <div class="col-md-6">
                            <div class="single-input-wrap">
                                <input type="text" name="name" class="single-input">
                                <label>Name</label>
                            </div>
                            <div class="single-input-wrap">
                                <input type="text" name="email" class="single-input">
                                <label>E-mail</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="single-input-wrap">
                                <textarea class="single-input textarea" name="message" cols="20"></textarea>
                                <label class="single-input-label">Message</label>
                            </div>
                        </div>
<!--                         <div class="col-12 text-center"> -->
<!--                             <a class="btn btn-green" href="#">Send</a> -->
<!--                         </div>  -->
                        <div class="form-group col-md-12">
						<button type="submit" name="upload" id="upload" value="Apply" 
    style="margin-left: 336px;"class="btn btn-green ">Submit</button>
					</div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>