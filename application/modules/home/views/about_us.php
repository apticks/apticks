 <!-- breadcrumb area start -->

<div class="breadcrumb-area" style="background-image:url(assets/master/img/page-title-bg.png);">

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <ul class="page-list">
                        <li><a href="<?php echo base_url();?>home">Home</a></li>
                        <li>About</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb area End -->


<div class="sbst-provide-security mg-top-100 mg-bottom-120" style="background-image:url(<?php echo base_url();?>assets/master/img/startup/why-choose/2.png);">
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-6">
                <div class="img-with-video img-with-video-2 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="img-wrap">
                        <img src="<?php echo base_url();?>assets/master/img/startup/why-choose/1.png" alt="video">
                        <div class="hover">
                            <a href="https://www.youtube.com/watch?v=c7XEhXZ_rsk" class="video-play-btn mfp-iframe"><img src="<?php echo base_url();?>assets/master/img/we-provide/3.png" alt="video"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 desktop-center-item">
                <div class="wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title">We is the <span>APTICKS</span></h2>
                       <p>It is the clear Leadership that Brings the Revolutions, not simply the Technology !</p>
                        <p>Apticks believes that the perfect Leadership alone will bring the revolutions, not simply the Technology. each skilled at Apticks has unique leadership qualities, WHO area unit determined, fearless, and skilful. Their spirit and values area unit contagious that inspire the complete team to deliver their best to our reputable purchasers.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- vission area start -->
    <div class="container">
        <div class="row">
            <div class="col-lg-6 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="img-wrap">
                        <img src="<?php echo base_url();?>assets/master/img/goal2.gif" alt="video">
                        
                    </div>            
            </div>
            <div class="col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title about">Our<span>Vission?</span></h2>
                        <p>Providing complete IT innovative solutions to form our customers and industry successful through determination, creativity, passion, productivity, value for money, ensuring our products of outstanding quality, instill pride of ownership by innovative and sustainable technical solutions.</p>
                        <p>We believe every software package are going to be a part of a platform ecosystem, and software expertise will become key to success within the digital world.</p>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

<!-- vission area End -->
<br>
<br>
<!-- mission area start -->
 <div class="container">
        <div class="row">
            <div class="col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title about">Our<span>Mission?</span></h2>
                        <p>The mission of the APTICKS  is to explore, understand and explain the origin and nature of service in the evolution of intelligence and
Enjoying innovation and applying technology for improving the lives of people as well as, Enchancing the image of worked in high technology.</p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Concept Design</span>
                            </div>
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Concept Implementation</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Concept Development</span>
                            </div>
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Testing work</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
             
                    <div class="img-wrap">
                        <img src="<?php echo base_url();?>assets/img/mission.png" alt="video">
                        
                    </div>
                
            
            </div>
            
        </div>
    </div>

<!-- mission area End -->
<!-- sbst service area start -->
<div class="sbst-service-area pd-top-80">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-12 col-lg-12">
                <div class="section-title text-center about">
                    <h2 class="title">What We <span>Do?</span></h2>
                    <p class="section-title "><span >APTICKS</span> providing unique services to its purchasers within the fields of Mobile Application Development, management, internet Application Development. Apticks is that the trusty whole by a number of most productive  firms as a result of we tend to deliver complete IT Solutions. Our knowledgeable team will instantly support your business outcomes with innovation.</p>
<p>We offer real product-thinking. We offer a novel combination of enterprise-scale product, technology situations, software package skill, and deeply established product.
</p>
                </div>
            </div>
        </div>
        <div class="row custom-gutters-20 justify-content-center">
            <div class="col-lg-3 col-sm-6">
                <div class="single-service-2 text-center about ">
                    <div class="thumb">
                        <img src="<?php echo base_url();?>assets/master/img/startup/service/1.png" alt="service">
                    </div>
                    <h6 class="about">Digital Marketing</h6>
                    <p>Do you want to covers advertising through online channels like search engines, websites, social media,and mobile apps.??Join Apticks!!!</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-service-2 text-center about">
                    <div class="thumb">
                        <img src="<?php echo base_url();?>assets/master/img/startup/service/2.png" alt="service">
                    </div>
                    
                    <h6 class="about">Web Development</h6>
                    <p>Apticks provides creation of application programs that reside on remote servers and area unit delivered to the user’s device.</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-service-2 text-center about">
                    <div class="thumb">
                        <img src="<?php echo base_url();?>assets/master/img/startup/service/3.png" alt="service">
                    </div>
                    
                    <h6 class="about">Graphic Design</h6>
                    <p>Apticks provides craft of creating visual content to communicate messages. Applying visual hierarchy and page layout techniques.</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-service-2 text-center about">
                    <div class="thumb">
                        <img src="<?php echo base_url();?>assets/master/img/startup/service/4.png" alt="service">
                    </div>
                    
                    <h6 class="about">Android Apps Development</h6>
                    <p>Apticks One of the best Software Development company.Apticks is the forefront as the most innovative.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- sbst service area end -->

<!-- team area Start 
<div class="team-area-wrpper about-team-area-wrpper pd-top-112 mg-top-120">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-10">
                <div class="section-title text-center">
                    <h2 class="title">Apticks Creative <span>Team</span></h2>
                    <p>Apticks One of the best Software Development company. Apticks is at the forefront as the most innovative web development company. Our cost efficient and structured teamwork.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="team-area text-center">
        <div class="container">
            <div class="row custom-gutters-20">
                <div class="col-lg-3 col-sm-6">
                    <div class="single-team">
                        <div class="thumb">
                            <img src="<?php echo base_url();?>assets/master/img/startup/team/1.jpg" alt="team">
                        </div>
                        <h6><a href="#">Abedin Abed</a></h6>
                        <span>Creative Director</span>
                        <p>Apticks One of the best Software Development company.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-team">
                        <div class="thumb">
                            <img src="<?php echo base_url();?>assets/master/img/startup/team/2.jpg" alt="team">
                        </div>
                        <h6><a href="#">Jabedin jabed</a></h6>
                        <span>Projector Manager</span>
                        <p>Apticks One of the best Software Development company.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-team">
                        <div class="thumb">
                            <img src="<?php echo base_url();?>assets/master/img/startup/team/5.png" alt="team">
                        </div>
                        <h6><a href="#">Moin Khan</a></h6>
                        <span>Art Director</span>
                        <p>Apticks One of the best Software Development company.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-team">
                        <div class="thumb">
                            <img src="<?php echo base_url();?>assets/master/img/startup/team/3.jpg" alt="team">
                        </div>
                        <h6><a href="#">Moin Khan</a></h6>
                        <span>Art Director</span>
                        <p>Apticks One of the best Software Development company.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 team area End -->
 <div  class="container">
  <div class="fullwidth">       
      <div class="container separator">
         <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-10">
                <div class="section-title text-center">
                    <h2 class="title wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.1s">Our Working <span>Progress?</span></h2>
                     </div>
            </div>
        

        <ul class="progress-tracker progress-tracker--text">
          <li class="progress-step is-complete">
            <div class="progress-marker"></div>
            <div class="progress-text progress-text--dotted progress-text--dotted-1">
              <h4 class="progress-title about"> Information Gathering</h4>
            
            </div>
          </li>

          <li class="progress-step is-complete">
            <div class="progress-marker"></div>
            <div class="progress-text progress-text--dotted progress-text--dotted-3">
              <h4 class="progress-title about"> Planning</h4>
             
            </div>
          </li>


          <li class="progress-step">
            <div class="progress-marker"></div>
            <div class="progress-text progress-text--dotted progress-text--dotted-4">
              <h4 class="progress-title about"> Design</h4>
            
            </div>
          </li>

          <li class="progress-step">
            <div class="progress-marker"></div>
            <div class="progress-text progress-text--dotted progress-text--dotted-6">
              <h4 class="progress-title about">  Development</h4>
            
            </div>
          </li>
            <li class="progress-step">
            <div class="progress-marker"></div>
            <div class="progress-text progress-text--dotted progress-text--dotted-8">
              <h4 class="progress-title about">Testing and Delievry</h4>
              
            </div>
          </li>
            <li class="progress-step">
            <div class="progress-marker"></div>
            <div class="progress-text progress-text--dotted progress-text--dotted-10">
              <h4 class="progress-title about">Maintenance</h4>
              
            </div>
          </li>
        </ul>        
      </div>
      </div>
    </div>
    </div>

<style type="text/css">
   .about{
    font-family: tinos;
    font-size: 20px;
   }

   .overlay1 {
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;

  overflow: hidden;
  width: 100%;
  height: 100%;
  -webkit-transform: scale(0);
  -ms-transform: scale(0);
  transform: scale(0);
  -webkit-transition: .3s ease;
  transition: .3s ease;
}

.container:hover .overlay1 {
  -webkit-transform: scale(1);
  -ms-transform: scale(1);
  transform: scale(1);
}

.text {
  color: red;
  font-size: 20px;
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  text-align: center;
} 
.card {
  position: absolute;
  top: 50%;
  left: 50%;
  width: 300px;
  height: 300px;
  margin: -150px;
  float: left;
  perspective: 500px;
}

.content {
  position: absolute;
  width: 100%;
  height: 100%;
  box-shadow: 0 0 15px rgba(0,0,0,0.1);

  transition: transform 1s;
  transform-style: preserve-3d;
}

.card:hover .content {
  transform: rotateY( 180deg ) ;
  transition: transform 0.5s;
}




}</style>