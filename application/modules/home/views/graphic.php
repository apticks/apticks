<!-- breadcrumb area start -->
<div class="breadcrumb-area" style="background-image:url(assets/master/img/page-title-bg.png);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">Graphic Design</h1>
                    <ul class="page-list">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li>Graphic Design</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--content area start-->
<div class="about-provide-area pd-top-120 bg-none">
    <div class="container">
        <div class="row">
             <div class="col-xl-7 col-lg-6">
                <div class="img-with-video">
                    <div class="img-wrap">
                        <img src="<?php echo base_url();?>assets/img/graphici.gif" alt="video" style="
    margin-top: 45px;
">

                    </div>
                </div>
            </div>

            <div class="col-xl-5 col-lg-6 desktop-center-item">
                
                    <div class="section-title style-two">
                     <p>APTICKS a years of multi tasking and multi disciplined design studio that gives strength to strength fresh concepts encompassing dedicated service levels, meeting all the creative needs of clients and organizations in India and overseas. Be it our graphic design, brochures, web templates, logo design, advertising, internet site design or packaging design at APTICKS we believe getting results, we are focused in getting the assured resultants as said to the client’s. We value our client’s efforts in believing us in getting the simplest of the work finished them. We are a very professional organisation who works in both in time and quality service.

Our master group including Graphic creators, Illustrators, and Infographics has been effectively giving custom realistic plans to varied customers situated within the India and different nations on the earth . We are refreshed with the foremost recent realistic planning patterns and forecasts without bounds and help our customers to remain refreshed with the foremost recent outlines. It’s the perfect opportunity for you choose the right visual computerization account assembling and build up your image now.</p>
                    </div>
                
            </div>
        </div>
    </div>
</div>
<!--content area end-->
<div class="sbst-service-area pd-top-80">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-12 col-lg-12">
                <div class="section-title text-center about">
                    <h2 class="title">What We <span>Do?</span></h2>
                    <p class="section-title ">At <span >APTICKS</span>, we plan pamphlets, inventories and other promoting insurance to be something aside from presents; they're instructive and deals devices made specifically to require your organization to the subsequent level. We utilize inventive designs blended with instinctive and proficient formats therefore the leaflets and lists we make will transform perusers into clients</p>

                </div>
            </div>
        </div>
        </div>
    </div>

   

    <div class="container">
        <div class="row">
            <div class="col-lg-6 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="img-wrap">
                       <img src="<?php echo base_url();?>assets/img/ui-ux-design.png" style="height: 334px;width: 339px;" >
                        
                    </div>            
            </div>
            <div class="col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title about">UX<span>Design</span></h2>
                        <p style="margin-left: -157px;">Client focused plan is merely a beginning stage. provided with bits of data from substance to the investigation, our iterative UX configuration process is driven by both expertise and enchantment. Thus, locales see enhanced client encounter including expanded client devotion, higher transformation rates, brought down relinquish rates and even diminished client benefit demands.</p>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
<br>
<br>
 <div class="row">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                        <h2 class="title about">Logo<span>Design</span></h2>
                        <p>We should Give your Business the right First Impression since you only get one.If you’re a start-up or a venture, Logo may be a key component for you. the brand is your corporate character. we provide tweaked administrations for organization logo plan that suites Start-ups, Mid-estimate organizations, and Enterprises. just in case you’re not content together with your present logo plan and it had been done once you began your business and now that you’re found out you would like the brand mention your prosperity, please catch on touch for rethinking your logo.</p>
                    </div>
                   
                </div>
            </div>
            <div class="col-lg-6 wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="img-wrap">
                       <img src="<?php echo base_url();?>assets/img/logod.png" >
                    </div>
            </div>
            
        </div>
    </div>

  
</div>