<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>


<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css">
<meta name="viewport" content="width=device-width">



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.min.js"></script>
<!-- breadcrumb area start -->
<div class="breadcrumb-area" style="background-image:url(assets/master/img/page-title-bg.png);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">Digital Marketing</h1>
                    <ul class="page-list">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li>Digital Marketing</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- offer area start -->
<div class="sbst-offer-area pd-top-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-11">
                <div class="img-with-video img-with-video-2 wow animated fadeInCenter" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="img-wrap">
                        <img src="<?php echo base_url();?>assets/master/img/digital.gif" alt="video" style="margin-bottom: 23px;">
                        
                    </div>
                </div>
            </div>
            <div class="col-xl-12 col-lg-12">
                <div class="section-title text-justify">
                    <h2 class="title wow animated fadeInUp text-center" data-wow-duration="0.6s" data-wow-delay="0.1s">Digital<span>Marketing?</span></h2>
                    <p class="wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.2s">Digital promoting relies on technology that is ever-evolving and fast-changing, a similar options ought to be expected from digital promoting developments and methods. This portion is an endeavor to qualify or segregate the notable highlights existing and getting used as of press time.</p>

<p class="wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.2s">Segmentation: a lot of focus has been placed on segmentation inside digital promoting, so as to focus on specific markets in each business-to-business and business-to-consumer sectors.
Influencer marketing: necessary nodes area unit known inside connected communities, called influencers. this is often changing into a crucial construct in digital targeting. it's doable to achieve influencers via paid advertising, like Facebook Advertising or Google Adwords campaigns, or through subtle sCRM (social client relationship management) computer code, like SAP C4C, Microsoft Dynamics, Sage CRM and Salesforce CRM. several universities currently focus, at Masters level, on engagement ways for influencers.</p>
                </div>
            </div>
        </div>
       <div class="row custom-gutters-20 justify-content-center">
            <div class="col-lg-3 col-sm-6">
                <div class="single-service-2 text-center about ">
                    <div class="thumb">
                        <img src="<?php echo base_url();?>assets/master/img/startup/service/1.png" alt="service">
                    </div>
                    
                    <h6 class="about">Google Adwords</h6>
                    <p>We’ll weed out your old and tired email addresses and make sure your list is squeaky clean and ready to score deliverability points</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-service-2 text-center about">
                    <div class="thumb">
                        <img src="<?php echo base_url();?>assets/master/img/startup/service/2.png" alt="service">
                    </div>
                    
                    <h6 class="about">E-mail marketing</h6>
                    <p>We will code your email in HTML and CSS that utilizes media queries to help achieve optimal formatting across all devices of all sizes.</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-service-2 text-center about">
                    <div class="thumb">
                        <img src="<?php echo base_url();?>assets/master/img/startup/service/3.png" alt="service">
                    </div>
                    
                    <h6 class="about">Social Media Marketing</h6>
                    <p>Our fully managed service includes the creation of two email campaigns per month, all professionally designed and written for you.</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-service-2 text-center about">
                    <div class="thumb">
                        <img src="<?php echo base_url();?>assets/master/img/startup/service/4.png" alt="service">
                    </div>
                    
                    <h6 class="about">
Search Engine Marketing</h6>
                    <p>Apticks marketing  will help you grow your business and convert more leads, all while saving your valuable time, money and heartache</p>
                </div>
            </div>
        </div>
    </div>
</div>
  
<!-- offer area End -->
