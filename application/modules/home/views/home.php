<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css">
<meta name="viewport" content="width=device-width">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.min.js"></script>
<!-- header area start -->
<div class="header-area sbst-banner-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-lg-6 col-md-7">
                <div class="header-inner-details">
                    <div class="header-inner">
                        <h2 class="title wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.1s">Creative, Smart,Passionate agency <span>APTICKS!!!</span></h2>
                            
                      <!--   <p class=" wow animated fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">Apticks believes that the perfect Leadership alone will bring the revolutions, not simply the Technology. each skilled at Apticks has unique leadership qualities, WHO area unit determined, fearless, and skilful. Their spirit and values area unit contagious that inspire the complete team to deliver their best to our reputable purchasers.</p> -->
                        <!-- <div class="btn-wrapper desktop-left padding-top-20 wow animated fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
                            <a href="#" class="btn btn-radius btn-green">Read More</a>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 offset-xl-1 hidden-sm">
                <div class="banner-animate-thumb">
                    <div class="header-img-1 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0s">
                        <img src="<?php echo base_url();?>assets/master/img/startup/slider/1.png" alt="banner-img">
                    </div>
                    <div class="header-img-2 wow animated fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
                        <img src="<?php echo base_url();?>assets/master/img/startup/slider/copy.jpg" alt="banner-img">
                    </div>
                    <div class="header-img-3 wow animated fadeInDown" data-wow-duration="1s" data-wow-delay="0.8s">
                        <img src="<?php echo base_url();?>assets/master/img/startup/slider/3.png" alt="banner-img">
                    </div>
                    <div class="header-img-4 wow animated fadeInDown" data-wow-duration="1s" data-wow-delay="1.0s">
                        <img src="<?php echo base_url();?>assets/master/img/startup/slider/4.png" alt="banner-img">
                    </div>
                    <div class="header-img-5 wow animated fadeInDown" data-wow-duration="1s" data-wow-delay="1.3s">
                        <img src="<?php echo base_url();?>assets/master/img/startup/slider/5.png" alt="banner-img">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- header area End -->
  <meta name="viewport" content="width=device-width, initial-scale=1">   
<!-- sbst service area start -->
<div class="sbst-service-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-10">
                <div class="section-title text-center">
                    <h2 class="title wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.1s">What We <span>Do?</span></h2>
                    <p class="wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.2s">We believe in building strong brands, good clean design, well-crafted content, and integrated strategies.</p>
                </div>
            </div>
        </div>
        <div class="row custom-gutters justify-content-justify">
        <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-4">
                <!-- Card Flip -->
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <!-- front content -->
                        <img src="<?php echo base_url();?>assets/img/software.png" alt="banner-img" style="height: 350px; width: 100%; display: block;">
                           <h4 class="card-title">Software Development</h4>
                        </div>
                         <div class="flip-card-back">
                          <h2 class="card-title">Software Development</h2>
                            <p>----------------------------------</p>
                          <p class="card-text ccontent">Software developer, also referred to as a programmer , you’ll be playing a key role within the design, installation, testing and maintenance of software systems. The creative minds behind computer programs.</p>
                          <a class="card-link" href="<?php echo base_url();?>software" style="margin-left: 92px;">Find out more!!!</a>
                        </div>
                    </div>
                </div>
                <!-- End Card Flip -->
            </div>
            <div class="col-sm-6 col-lg-4">
                <!-- Card Flip -->
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <!-- front content -->
                            <img src="<?php echo base_url();?>assets/img/mob1.png" alt="banner-img" style="height: 310px; width: 100%; display: block;">
                                <h4 class="card-title">Mobile App Development</h4>
                        </div>
                              <div class="flip-card-back">
                                <h4 class="card-title">Mobile App Development</h4>
                                  <p>----------------------------------</p>
                                <p class="card-text ccontent">APTICKS Mobile application are custom-manufactured, compact arrangements and administrations to new companies by utilizing our times of experience delivering astounding advanced items.</p>
                                <a class="card-link" href="<?php echo base_url();?>mobile" style="margin-left: 92px;">Find out more!!!</a>
                              </div>
                     </div>
                </div>
                <!-- End Card Flip -->
            </div>
              <div class="col-sm-6 col-lg-4">
                <!-- Card Flip -->
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <!-- front content -->
                            <img src="<?php echo base_url();?>assets/img/website.jpg" alt="banner-img" style="height: 350px; width: 100%; display: block;">
                           
                                <h4 class="card-title">Web Development</h4>
                        </div>
                              <div class="flip-card-back">
                                <h4 class="card-title">Web Development</h4>
                                  <p>----------------------------------</p>
                                <p class="card-text ccontent">Web application development can generally have a brief development life-cycle lead by a little development team. Front-end development for net applications is accomplished through client-side programming.</p>
                                <a class="card-link" href="<?php echo base_url();?>web" style="margin-left: 92px;">Find out more!!!</a>
                              </div>
                     </div>
                </div>
                <!-- End Card Flip -->
            </div>
           
        </div>
    </div>
</div>
</div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>


<div class="sbst-service-area">
<div class="container">
         <div class="row custom-gutters justify-content-justify">
       
    <div class="container">
        <div class="row">
             <div class="col-sm-6 col-lg-4">
                <!-- Card Flip -->
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <!-- front content -->
                        <img src="<?php echo base_url();?>assets/img/digital.png" alt="banner-img" style="height: 350px; width: 100%; display: block;">
                           <h4 class="card-title">Digital Marketing</h4>
                        </div>
                         <div class="flip-card-back">
                          <h2 class="card-title">Digital Marketing</h2>
                            <p>----------------------------------</p>
                          <p class="card-text ccontent">Digital promoting relies on technology that is ever-evolving and fast-changing, a similar options to be expected from digital promoting developments and methods. APTICKS marketing  will help you grow your business.</p>
                          <a class="card-link" href="<?php echo base_url();?>digital" style="margin-left: 92px;">Find out more!!!</a>
                        </div>
                    </div>
                </div>
                <!-- End Card Flip -->
            </div>
            <div class="col-sm-6 col-lg-4">
                <!-- Card Flip -->
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <!-- front content -->
                        <img src="<?php echo base_url();?>assets/img/graphics.png" alt="banner-img" style="height: 350px; width: 100%; display: block;">
                           <h4 class="card-title">Graphics Design</h4>
                        </div>
                         <div class="flip-card-back">
                          <h2 class="card-title">Graphics Design</h2>
                            <p>----------------------------------</p>
                          <p class="card-text ccontent">APTICKS a years of multi tasking and multi disciplined design studio that gives strength to strength fresh concepts encompassing dedicated service levels, meeting all the creative needs of clients and organizations in India and overseas.</p>
                          <a class="card-link" href="<?php echo base_url();?>graphic" style="margin-left: 92px;">Find out more!!!</a>
                        </div>
                    </div>
                </div>
                <!-- End Card Flip -->
            </div>
            <div class="col-sm-6 col-lg-4">
                <!-- Card Flip -->
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <!-- front content -->
                        <img src="<?php echo base_url();?>assets/img/test.png" alt="banner-img" style="height: 350px; width: 100%; display: block;">
                           <h4 class="card-title">Software Testing</h4>
                        </div>
                         <div class="flip-card-back">
                          <h2 class="card-title">Software Testing</h2>
                            <p>----------------------------------</p>
                          <p class="card-text ccontent">APTICKS providing platform for changing market demands  to convey fantastic programming at fast. Web testing could be a code testing follow to check net sites or web applications for potential bugs.</p>
                          <a class="card-link" href="<?php echo base_url();?>testing" style="margin-left: 92px;">Find out more!!!</a>
                        </div>
                    </div>
                </div>
                <!-- End Card Flip -->
            </div>
          </div>
    </div>
</div>     
 </div>
    </div>

<!-- sbst service area end -->

<div class="sbst-provide-security mg-top-100 mg-bottom-120" style="background-image:url(<?php echo base_url();?>assets/master/img/startup/why-choose/2.png);">
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-6">
                <div class="img-with-video img-with-video-2 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="img-wrap">
                        <img src="<?php echo base_url();?>assets/master/img/startup/why-choose/1.png" alt="video">
                        <div class="hover">
                            <a href="https://www.youtube.com/watch?v=c7XEhXZ_rsk" class="video-play-btn mfp-iframe"><img src="<?php echo base_url();?>assets/master/img/we-provide/3.png" alt="video"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 desktop-center-item">
                <div class="wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two">
                        <h2 class="title">Why Choose <span>APTICKS?</span></h2>
                        <p>APTICKS One of the best Software Development company. Our co-creation framework and technology  guides choice of right technologies and architecture and builds future-ready products. Our clients count on our offshore business solutions on a regular basis. From concept design to development, testing & implementation, our team is here to support and guide you every step of the way.</p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Team Work</span>
                            </div>
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Concept Implementation</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Good Job Invironment</span>
                            </div>
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Work Freedom</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="sbst-skill-area pd-top-120">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-lg-6 desktop-center-item">
                <div class="animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two">
                        <h2 class="title">We Have Strong <span>Skill</span></h2>
                        <p>APTICKS is at the forefront as the most innovative web development company. Our cost efficient and structured teamwork justifies.</p>
                    </div>
                    <div class="all-progress" id="progress-running">
                        <div class="progress-item">
                            <span class="progress-heading">User Interface Design</span>
                            <span class="progress-count">95%</span>
                            <div  class="progress-bg">
                                <div id='progress-one' class="progress-rate" data-value='95'>
                                </div>
                            </div>
                        </div>
                        <div class="progress-item">
                            <span class="progress-heading">User Experience</span>
                            <span class="progress-count">86%</span>
                            <div  class="progress-bg">
                                <div id='progress-two' class="progress-rate" data-value='86'>
                                </div>
                            </div>
                        </div>
                        <div class="progress-item">
                            <span class="progress-heading">Market Analysis</span>
                            <span class="progress-count">90%</span>
                            <div  class="progress-bg">
                                <div id='progress-three' class="progress-rate" data-value='90'>
                                </div>
                            </div>
                        </div>
                        <div class="progress-item mg-bottom-0-991">
                            <span class="progress-heading">Market Analysis</span>
                            <span class="progress-count">80%</span>
                            <div  class="progress-bg">
                                <div id='progress-four' class="progress-rate" data-value='80'>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 offset-xl-1 col-lg-6">
                <div class="shape-bg-image animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="thumb">
                        <img src="<?php echo base_url();?>assets/master/img/startup/skill/1.png" alt="service">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- offer area start -->
<div class="sbst-offer-area pd-top-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-10">
                <div class="section-title text-center">
                    <h2 class="title wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.1s">What We are <span>Offering?</span></h2>
                    <p class="wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.2s">We want to determine AN open and honest partnership with each customer that uses our services. operating along aspect by aspect, we try to introduce third-party services, new code tools and management solutions that not solely yield bigger transactional revenue through enhanced early engagement and upsells, however leave a business’s customers happy when each complete interaction.</p>
                </div>
            </div>
        </div>
        <div class="row custom-gutters-28 justify-content-center">
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="single-offer wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.1s">
                    <div class="media">
                        <img src="<?php echo base_url();?>assets/master/img/startup/offer/1.png" alt="offer">
                        <div class="media-body align-self-center">
                            <h6>Challenging Project</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="single-offer wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.3s">
                    <div class="media">
                        <img src="<?php echo base_url();?>assets/master/img/startup/offer/2.png" alt="offer">
                        <div class="media-body align-self-center">
                            <h6>Competitive salary</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="single-offer wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s">
                    <div class="media">
                        <img src="<?php echo base_url();?>assets/master/img/startup/offer/3.png" alt="offer">
                        <div class="media-body align-self-center">
                            <h6>Play Zone</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="single-offer wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.7s">
                    <div class="media">
                        <img src="<?php echo base_url();?>assets/master/img/startup/offer/4.png" alt="offer">
                        <div class="media-body align-self-center">
                            <h6>Strong Team</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="single-offer wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.8s">
                    <div class="media">
                        <img src="<?php echo base_url();?>assets/master/img/startup/offer/5.png" alt="offer">
                        <div class="media-body align-self-center">
                            <h6>Decorated Office</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="single-offer wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="1.0s">
                    <div class="media">
                        <img src="<?php echo base_url();?>assets/master/img/startup/offer/6.png" alt="offer">
                        <div class="media-body align-self-center">
                            <h6>Free Coffee &amp; Snacks</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- offer area End -->

<!-- team area start -->
<!-- <div class="team-area-wrpper sbst-team-area-wrpper">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-10">
                <div class="section-title text-center">
                    <h2 class="title wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.1s">Riyaqas Creative <span>Team</span></h2>
                    <p class="wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.2s">Riyaqas One of the best Software Development company. Riyaqas is at the forefront as the most innovative web development company. Our cost efficient and structured teamwork.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="team-area text-center">
        <div class="container">
            <div class="row custom-gutters-20">
                <div class="col-lg-3 col-sm-6">
                    <div class="single-team wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.1s">
                        <div class="thumb">
                            <img src="<?php echo base_url();?>assets/master/img/startup/team/1.jpg" alt="team">
                        </div>
                        <h6><a href="#">Sarathkumar</a></h6>
                        <span>Android Developer</span>
                        <p>Riyaqas One of the best Software Development company.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-team wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.3s">
                        <div class="thumb">
                            <img src="<?php echo base_url();?>assets/master/img/startup/team/2.jpg" alt="team">
                        </div>
                        <h6><a href="#">Jabedin jabed</a></h6>
                        <span>Projector Manager</span>
                        <p>Riyaqas One of the best Software Development company.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-team wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s">
                        <div class="thumb">
                            <img src="<?php echo base_url();?>assets/master/img/startup/team/3.jpg" alt="team">
                        </div>
                        <h6><a href="#">Moin Khan</a></h6>
                        <span>Art Director</span>
                        <p>Riyaqas One of the best Software Development company.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-team wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.7s">
                        <div class="thumb">
                            <img src="<?php echo base_url();?>assets/master/img/startup/team/5.png" alt="team">
                        </div>
                        <h6><a href="#">Raysa Rabi</a></h6>
                        <span>Marketing Exe.</span>
                        <p>Riyaqas One of the best Software Development company.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- team area end -->

<!-- client area start -->
<div class="sbst-client-area pd-top-112">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-10">
                <div class="section-title text-center">
                    <h2 class="title">Our valuable <span>Client</span></h2>
                    <p>The relationship between a manufacturer and his advertising agency is almost as intimate as the relationship between a patient and his doctor. Make sure that you can live happily with your prospective client before you accept his account.
</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="client-slider-2">
                    <div class="client-slider-2-item">
                        <a href="#" class="thumb">
                            <img src="<?php echo base_url();?>assets/master/img/startup/client/clientfirst.png" alt="client" style="max-width: 251px;margin-left: 322px;">
                        </a>
                    </div>
                    
                   <!--  <div class="client-slider-2-item">
                        <a href="#" class="thumb">
                            <img src="<?php //echo base_url();?>assets/master/img/startup/client/1.png" alt="client">
                        </a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- client area End -->

<!-- testimonial area start -->
<!-- <div class="sbst-testimonial-area mg-top-110">
    <div class="container">
        <div class="testimonial-slider2-bg">
            <div class="row justify-content-center">
                <div class="col-xl-10">
                    <div class="testimonial-slider-2">
                        <div class="item">
                            <div class="media text-center">
                                <div class="media-left">
                                    <img src="<?php echo base_url();?>assets/master/img/business/testimonial/2.png" alt="client">
                                </div>
                                <div class="media-body">
                                    <p>Our support team will get assistance from AI-powered suggestions, making it quicker than ever to handle support requests.</p>
                                    <h6>Mr.Mehar</h6>
                                    <span>CEO at Uxa</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="media text-center">
                                <div class="media-left">
                                    <img src="<?php echo base_url();?>assets/master/img/business/testimonial/3.png" alt="client">
                                </div>
                                <div class="media-body">
                                    <p>Our support team will get assistance from AI-powered suggestions, making it quicker than ever to handle support requests.</p>
                                    <h6>Wilson jaq</h6>
                                    <span>CEO at kst</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="media text-center">
                                <div class="media-left">
                                    <img src="<?php echo base_url();?>assets/master/img/business/testimonial/4.png" alt="client">
                                </div>
                                <div class="media-body">
                                    <p>Our support team will get assistance from AI-powered suggestions, making it quicker than ever to handle support requests.</p>
                                    <h6>R. Jenkins</h6>
                                    <span>Founder of Uxa</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- testimonial area End 
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag 

<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Project counter</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp'>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css'>

      <link rel="stylesheet" href="css/style.css">

  
</head>

<body>

  <div class="sectiontitle">
    <h2>Projects statistics</h2>
    <span class="headerLine"></span>
</div>
<div id="projectFacts" class="sectionClass">
    <div class="fullWidth eight columns">
        <div class="projectFactsWrap ">
            <div class="item wow fadeInUpBig animated animated" data-number="12" style="visibility: visible;">
                <i class="fa fa-briefcase"></i>
                <p id="number1" class="number">12</p>
                <span></span>
                <p>Projects done</p>
            </div>
            <div class="item wow fadeInUpBig animated animated" data-number="55" style="visibility: visible;">
                <i class="fa fa-smile-o"></i>
                <p id="number2" class="number">55</p>
                <span></span>
                <p>Happy clients</p>
            </div>
            <div class="item wow fadeInUpBig animated animated" data-number="359" style="visibility: visible;">
                <i class="fa fa-coffee"></i>
                <p id="number3" class="number">359</p>
                <span></span>
                <p>Cups of coffee</p>
            </div>
            <div class="item wow fadeInUpBig animated animated" data-number="246" style="visibility: visible;">
                <i class="fa fa-camera"></i>
                <p id="number4" class="number">246</p>
                <span></span>
                <p>Photos taken</p>
            </div>
        </div>
    </div>
</div>


  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

  

    <script  src="js/index.js"></script>




</body>

</html>

<style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=PT+Sans+Narrow);
body {
  font-family: Open Sans, "Helvetica Neue", "Helvetica", Helvetica, Arial,   sans-serif;
  font-size: 13px;
  color: black;
  position: relative;
  -webkit-font-smoothing: antialiased;
  margin: 0;
}

* {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}

body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, p, blockquote, th, td {
  margin: 0;
  padding: 0;
  font-size: 13px;
  direction: ltr;
}

.sectionClass {
  padding: 20px 0px 50px 0px;
  position: relative;
  display: block;
}

.fullWidth {
  width: 100% !important;
  display: table;
  float: none;
  padding: 0;
  min-height: 1px;
  height: 100%;
  position: relative;
}


.sectiontitle {
  background-position: center;
  margin: 30px 0 0px;
  text-align: center;
  min-height: 20px;
}

.sectiontitle h2 {
  font-size: 30px;
  color: #222;
  margin-bottom: 0px;
  padding-right: 10px;
  padding-left: 10px;
}


.headerLine {
  width: 160px;
  height: 2px;
  display: inline-block;
  background: #101F2E;
}


.projectFactsWrap{
    display: flex;
  margin-top: 30px;
  flex-direction: row;
  flex-wrap: wrap;
}


#projectFacts .fullWidth{
  padding: 0;
}

.projectFactsWrap .item{
  width: 25%;
  height: 100%;
  padding: 50px 0px;
  text-align: center;
}

.projectFactsWrap .item:nth-child(1){
  background: rgb(222, 27, 27);

}

.projectFactsWrap .item:nth-child(2){
  background: rgb(222, 27, 27);
}

.projectFactsWrap .item:nth-child(3){
  background: rgb(222, 27, 27);
}

.projectFactsWrap .item:nth-child(4){
  background: rgb(222, 27, 27);
}

.projectFactsWrap .item p.number{
  font-size: 40px;
  padding: 0;
  font-weight: bold;
}

.projectFactsWrap .item p{
  color: rgba(23, 21, 21, 0.8);
  font-size: 18px;
  margin: 0;
  padding: 10px;
  font-family: 'Open Sans';
}


.projectFactsWrap .item span{
  width: 60px;
  background: rgba(255, 255, 255, 0.8);
  height: 2px;
  display: block;
  margin: 0 auto;
}


.projectFactsWrap .item i{
  vertical-align: middle;
  font-size: 50px;
  color: rgba(23, 21, 21, 0.8);
}


.projectFactsWrap .item:hover i, .projectFactsWrap .item:hover p{
  color: white;
}

.projectFactsWrap .item:hover span{
  background: white;
}

@media (max-width: 786px){
  .projectFactsWrap .item {
     flex: 0 0 50%;
  }
}

/* AUTHOR LINK */


footer{
  z-index: 100;
  padding-top: 50px;
  padding-bottom: 50px;
  width: 100%;
  bottom: 0;
  left: 0;
}

footer p {
color: rgba(255, 255, 255, 0.8);
  font-size: 16px;
  opacity: 0;
  font-family: 'Open Sans';
  width: 100%;
    word-wrap: break-word;
  line-height: 25px;
  -webkit-transform: translateX(-200px);
  transform: translateX(-200px);
  margin: 0;
  -webkit-transition: all 250ms ease;
  -moz-transition: all 250ms ease;
  transition: all 250ms ease;
}

footer .authorWindow a{
  color: white;
  text-decoration: none;
}

footer p strong {
    color: rgba(255, 255, 255, 0.9);
}

.about-me-img {
  width: 120px;
  height: 120px;
  left: 10px;
  /* bottom: 30px; */
  position: relative;
  border-radius: 100px;
}


.about-me-img img {
}


.authorWindow{
  width: 600px;
  background: #75439a;
  padding: 22px 20px 22px 20px;
  border-radius: 5px;
  overflow: hidden;
}

.authorWindowWrapper{
  display: none;
  left: 110px;
  top: 0;
  padding-left: 25px;
  position: absolute;
}





.trans{
  opacity: 1;
  -webkit-transform: translateX(0px);
  transform: translateX(0px);
  -webkit-transition: all 500ms ease;
  -moz-transition: all 500ms ease;
  transition: all 500ms ease;
}

@media screen and (max-width: 768px) {
    .authorWindow{
         width: 210px;
    }

    .authorWindowWrapper{
             bottom: -170px;
  margin-bottom: 20px;
    }

    footer p{
          font-size: 14px;
    }
}



</style><script type="text/javascript">
        $.fn.jQuerySimpleCounter = function( options ) {
        var settings = $.extend({
            start:  0,
            end:    100,
            easing: 'swing',
            duration: 400,
            complete: ''
        }, options );

        var thisElement = $(this);

        $({count: settings.start}).animate({count: settings.end}, {
            duration: settings.duration,
            easing: settings.easing,
            step: function() {
                var mathCount = Math.ceil(this.count);
                thisElement.text(mathCount);
            },
            complete: settings.complete
        });
    };


$('#number1').jQuerySimpleCounter({end: 12,duration: 3000});
$('#number2').jQuerySimpleCounter({end: 55,duration: 3000});
$('#number3').jQuerySimpleCounter({end: 359,duration: 2000});
$('#number4').jQuerySimpleCounter({end: 246,duration: 2500});
</script>--->
<style type="text/css">.about{
    font-family: tinos;
    font-size: 20px;
   } 
   </style>
   <!--new counter---->
   <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->


<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
<div class="sbst-client-area pd-top-112">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-10">
                <div class="section-title text-center">
                    <h2 class="title">Our Statistics</h2>
                    <p>counter to count up to a target number</p>
                </div>
            </div>
        </div>
       
    </div>
</div>
<div class="container">
        <div class="row text-center">
            <div class="col">
            <div class="counter">
      <i class="fa fa-code fa-2x"></i>
      <h2 class="timer count-title count-number" data-to="100" data-speed="1500"></h2>
       <p class="count-text ">Our Customer</p>
    </div>
            </div>
              <div class="col">
               <div class="counter">
      <i class="fa fa-coffee fa-2x"></i>
      <h2 class="timer count-title count-number" data-to="1700" data-speed="1500"></h2>
      <p class="count-text ">Happy Clients</p>
    </div>
              </div>
              <div class="col">
                  <div class="counter">
      <i class="fa fa-lightbulb-o fa-2x"></i>
      <h2 class="timer count-title count-number" data-to="11900" data-speed="1500"></h2>
      <p class="count-text ">Project Complete</p>
    </div></div>
              <div class="col">
              <div class="counter">
      <i class="fa fa-bug fa-2x"></i>
      <h2 class="timer count-title count-number" data-to="157" data-speed="1500"></h2>
      <p class="count-text ">Coffee With Clients</p>
    </div>
              </div>
         </div>
</div>
<style type="text/css">
    .counter {
    background-color:#f5f5f5;
    padding: 20px 0;
    border-radius: 5px;
}

.count-title {
    font-size: 40px;
    font-weight: normal;
    margin-top: 10px;
    margin-bottom: 0;
    text-align: center;
  
    
}

.count-text {
    font-size: 13px;
    font-weight: normal;
    margin-top: 10px;
    margin-bottom: 0;
    text-align: center;
    color: #e41f0b;

}

.fa-2x {
    margin: 0 auto;
    float: none;
    display: table;
    color: #e41f0b;
}
.go{
    color: #fff;
    background-color: #d20523;
    border-color: #080808;
}
</style>
<script type="text/javascript">
    (function ($) {
    $.fn.countTo = function (options) {
        options = options || {};
        
        return $(this).each(function () {
            // set options for current element
            var settings = $.extend({}, $.fn.countTo.defaults, {
                from:            $(this).data('from'),
                to:              $(this).data('to'),
                speed:           $(this).data('speed'),
                refreshInterval: $(this).data('refresh-interval'),
                decimals:        $(this).data('decimals')
            }, options);
            
            // how many times to update the value, and how much to increment the value on each update
            var loops = Math.ceil(settings.speed / settings.refreshInterval),
                increment = (settings.to - settings.from) / loops;
            
            // references & variables that will change with each update
            var self = this,
                $self = $(this),
                loopCount = 0,
                value = settings.from,
                data = $self.data('countTo') || {};
            
            $self.data('countTo', data);
            
            // if an existing interval can be found, clear it first
            if (data.interval) {
                clearInterval(data.interval);
            }
            data.interval = setInterval(updateTimer, settings.refreshInterval);
            
            // initialize the element with the starting value
            render(value);
            
            function updateTimer() {
                value += increment;
                loopCount++;
                
                render(value);
                
                if (typeof(settings.onUpdate) == 'function') {
                    settings.onUpdate.call(self, value);
                }
                
                if (loopCount >= loops) {
                    // remove the interval
                    $self.removeData('countTo');
                    clearInterval(data.interval);
                    value = settings.to;
                    
                    if (typeof(settings.onComplete) == 'function') {
                        settings.onComplete.call(self, value);
                    }
                }
            }
            
            function render(value) {
                var formattedValue = settings.formatter.call(self, value, settings);
                $self.html(formattedValue);
            }
        });
    };
    
    $.fn.countTo.defaults = {
        from: 0,               // the number the element should start at
        to: 0,                 // the number the element should end at
        speed: 1000,           // how long it should take to count between the target numbers
        refreshInterval: 100,  // how often the element should be updated
        decimals: 0,           // the number of decimal places to show
        formatter: formatter,  // handler for formatting the value before rendering
        onUpdate: null,        // callback method for every time the element is updated
        onComplete: null       // callback method for when the element finishes updating
    };
    
    function formatter(value, settings) {
        return value.toFixed(settings.decimals);
    }
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
    formatter: function (value, options) {
      return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
    }
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
    var $this = $(this);
    options = $.extend({}, options || {}, $this.data('countToOptions') || {});
    $this.countTo(options);
  }
});
document.querySelector(".card-flip").classList.toggle("flip");

/* 
 * Holder.js for demo image
 * Just for demo purpose
 */
Holder.addTheme('gray', {
  bg: '#777',
  fg: 'rgba(255,255,255,.75)',
  font: 'Helvetica',
  fontweight: 'normal'
});
</script>

<style>
    .card-flip {
  perspective: 1000px;
  &:hover .flip,
  &.hover .flip {
    transform: rotateY(180deg);
  }
}

.card-flip,
.front,
.back {
  width: 100%;
  height: 480px;
}

.flip {
  transition: 0.6s;
  transform-style: preserve-3d;
  position: relative;
}

.front,
.back {
  backface-visibility: hidden;
  position: absolute;
  top: 0;
  left: 0;
}

.front {
  z-index: 2;
  transform: rotateY(0deg);
}

.back {
  transform: rotateY(180deg);
}
</style>
<style type="text/css">
  .flip-card {
  background-color: transparent;
  width: 300px;
  height: 300px;
  perspective: 1000px;
}

.flip-card-inner {
  position: relative;
  width: 120%;
  height: 127%;
  text-align: center;
  transition: transform 0.6s;
  transform-style: preserve-3d;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
}

.flip-card:hover .flip-card-inner {
  transform: rotateY(180deg);
}

.flip-card-front, .flip-card-back {
  position: absolute;
  width: 100%;
  height: 70%;
  
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
}

.flip-card-front {
  background-color: white;
  color: black;
}

.flip-card-back {
  background-color: white;
  color: red;
  transform: rotateY(180deg);
}
.ccontent{
font-size: 20px;
font-weight: 20px;

    text-align: center;
    padding: 20px;
}
.card-link{
  
  color:red;
}
</style>  

