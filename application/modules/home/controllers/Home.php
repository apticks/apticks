<?php

class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->template = 'template/master/main';
        $this->load->model('contact_model');
    }
    
    public function index(){
        $this->data['title'] = 'home';
        $this->data['content'] = 'home/home';
        $this->_render_page($this->template, $this->data);
    }
     public function about_us(){
        $this->data['title'] = 'about_us';
        $this->data['content'] = 'home/about_us';
        $this->_render_page($this->template, $this->data);
    }
    public function digital(){
        $this->data['title'] = 'digital';
        $this->data['content'] = 'home/digital';
        $this->_render_page($this->template, $this->data);
    }
    public function graphic(){
        $this->data['title'] = 'graphic';
        $this->data['content'] = 'home/graphic';
        $this->_render_page($this->template, $this->data);
    }
    public function software(){
        $this->data['title'] = 'software';
        $this->data['content'] = 'home/software';
        $this->_render_page($this->template, $this->data);
    }public function testing(){
        $this->data['title'] = 'about_us';
        $this->data['content'] = 'home/testing';
        $this->_render_page($this->template, $this->data);
    }public function web(){
        $this->data['title'] = 'web';
        $this->data['content'] = 'home/web';
        $this->_render_page($this->template, $this->data);
    }
    public function mobile(){
        $this->data['title'] = 'mobile';
        $this->data['content'] = 'home/mobile';
        $this->_render_page($this->template, $this->data);
    }
    public function contact(){
        $this->data['title'] = 'contact';
        $this->data['content'] = 'home/contact';
        $this->_render_page($this->template, $this->data);
        $this->form_validation->set_rules($this->contact_model->rules);
       
        if ($this->form_validation->run() == FALSE) {
           
        } else {
            $id = $this->contact_model->insert([
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'message' => $this->input->post('message'),
            ]);
            return sendEmail(NULL,'tptrupti011@gmail.com', 'Contact', 'Contact', NULL, NULL, NULL, NULL, NULL);
            
        }
    }
    public function privacy(){
        $this->data['title'] = 'privacy';
        $this->data['content'] = 'home/privacy';
        $this->_render_page($this->template, $this->data);
    }
    public function terms(){
        $this->data['title'] = 'terms';
        $this->data['content'] = 'home/terms';
        $this->_render_page($this->template, $this->data);
    }
}

