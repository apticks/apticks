<?php

class News extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        
        $this->template = 'template/admin/main';
        if (! $this->ion_auth->logged_in())
            redirect('auth/login');
        
        $this->load->model('news_model');
        $this->load->model('news_category_model');
    }
    
    /**
     * @author Trupti
     * @desc To Manage News Categories
     * @param string $type
     */
    public function news_categories($type = 'r'){
        /* if (! $this->ion_auth_acl->has_permission('news_categories'))
            redirect('admin'); */
            
            if ($type == 'c') {
                $this->form_validation->set_rules($this->news_category_model->rules);
                if (empty($_FILES['file']['name'])) {
                    $this->form_validation->set_rules('file', 'News Category Image', 'required');
                }
                if ($this->form_validation->run() == FALSE) {
                    $this->news_categories('r');
                } else {
                    $id = $this->news_category_model->insert([
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc')
                    ]);
                    
                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $this->file_up("file", "news_category", $id, '', 'no');
                    redirect('news_categories/r', 'refresh');
                }
            } elseif ($type == 'r') {
                $this->data['title'] = 'News';
                $this->data['content'] = 'news/category';
                $this->data['news_categories'] = $this->news_category_model->order_by('id', 'ASCE')->get_all();
                $this->_render_page($this->template, $this->data);
            }  elseif ($type == 'u') {
                $this->form_validation->set_rules($this->news_category_model->rules);
                if ($this->form_validation->run() == FALSE) {
                    echo validation_errors();
                } else {
                    $this->news_category_model->update([
                        'id' => $this->input->post('id'),
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc')
                    ], 'id');
                    
                    if ($_FILES['file']['name'] !== '') {
                        $path = $_FILES['file']['name'];
                        $ext = pathinfo($path, PATHINFO_EXTENSION);
                        $this->file_up("file", "news_category", $this->input->post('id'), '', 'no');
                    }
                    redirect('news_categories/r', 'refresh');
                }
            }elseif ($type == 'd') {
                echo $this->news_category_model->delete(['id' => $this->input->post('id')]);
            }elseif($type == 'edit'){
                $this->data['title'] = 'Edit News Category';
                $this->data['content'] = 'news/edit';
                $this->data['type'] = 'news_categories';
                $this->data['category'] = $this->news_category_model->where('id',$this->input->get('id'))->get();
                $this->data['i'] = $this->news_category_model->where('file',$this->input->get('file'))->get();
                $this->data['news_categories'] = $this->news_category_model->order_by('id', 'DESC')->where('id', $this->input->get('id'))->get();
                $this->_render_page($this->template, $this->data);
            }
    }
    
    /**
     * @author Mehar
     * @desc To Manage News
     * 
     * @param string $type
     */
    public function news($type = 'r'){
        /* if (! $this->ion_auth_acl->has_permission('news'))
            redirect('admin'); */
            
            if ($type == 'c') {
                $this->form_validation->set_rules($this->news_model->rules);
                if (empty($_FILES['file']['name'])) {
                    $this->form_validation->set_rules('file', 'News Image', 'required');
                }
                if ($this->form_validation->run() == FALSE) {
                    $this->data['title'] = 'News Add';
                    $this->data['content'] = 'news/add_news';
                    $this->data['news_categories'] = $this->news_category_model->order_by('id', 'DESC')->where('status', 1)->get_all();
                    $this->_render_page($this->template, $this->data);
                } else {
                    $id = $this->news_model->insert([
                        'title' => $this->input->post('title'),
                        'news_date' => $this->input->post('news_date'),
                        'video_link' => $this->input->post('url'),
                        'category' => $this->input->post('category'),
                        'news' => $this->input->post('news'),
                    ]);
                    $path = $_FILES['file']['name'];
                    $this->file_up("file", "news", $id, '', 'no');
                    redirect('news/r', 'refresh');
                }
            } elseif ($type == 'r') {
                $this->data['title'] = 'News';
                $this->data['content'] = 'news/news';
                $this->data['news'] = $this->news_model->order_by('id', 'DESC')->where('status', 1)->get_all();
                $this->data['news_categories'] = $this->news_category_model->order_by('id', 'DESC')->where('status', 1)->get_all();
                $this->_render_page($this->template, $this->data);
            } elseif ($type == 'u') {
                $this->form_validation->set_rules($this->news_model->rules);
                if ($this->form_validation->run() == FALSE) {
                    $this->news('edit');
                } else {
                    $this->news_model->update([
                        'id' => $this->input->post('id'),
                        'title' => $this->input->post('title'),
                        'news_date' => $this->input->post('news_date'),
                        'video_link' => $this->input->post('url'),
                        'category' => $this->input->post('category'),
                        'news' => $this->input->post('news'),
                    ], 'id');
                    
                    if ($_FILES['file']['name'] !== '') {
                        $path = $_FILES['file']['name'];
                        $this->file_up("file", "news", $this->input->post('id'), '', 'no');
                    }
                    redirect('news/r', 'refresh');
                }
            } elseif ($type == 'd') {
                echo $this->news_model->delete(['id' => $this->input->post('id')]);
            }elseif($type == 'edit'){
                $this->data['title'] = 'Edit News';
                $this->data['content'] = 'news/edit';
                $this->data['type'] = 'news';
                $this->data['news'] = $this->news_model->where('id',$this->input->get('id'))->get();
                $this->data['news_category'] = $this->news_category_model->where('status', 1)->get_all();
                $this->_render_page($this->template, $this->data);
            }
    }
}

