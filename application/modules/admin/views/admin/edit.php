<?php if($type == 'user_services'){?>

                <!--Edit State -->
                <div class="row">
                    <div class="col-12">
                        <h4>Edit Service</h4>
                        <form class="needs-validation" novalidate="" action="<?php echo base_url('user_services/u');?> " method="post" enctype="multipart/form-data">
                            <div class="card-header">
                                <div class="form-row">
                                    <div class="form-group col-md-6">

                                        <label>Service Name</label>
                                        <input type="text" name="name" class="form-control" required="" value="<?php echo $services['name']; ?>">

                                        <div class="invalid-feedback">Enter Valid Service Name?</div>
                                    </div>
                                    <input type="hidden" name="id" value="<?php echo $services['id'] ; ?>">
                                    </br>
                                    <div class="form-group col-md-6">
                                        <button class="btn btn-primary mt-27 ">Update</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                  <?php }?>